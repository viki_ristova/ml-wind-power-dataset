import numpy as np
from sklearn import datasets, linear_model
import matplotlib.pyplot as plt
import pandas as pd

from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
def import_data():
    raw_data_df=pd.read_excel('Wind_power_plant_hourly_2015-2017.xlsx')
    return raw_data_df



def create_dataset(dataset,look_back=1):
    dataX,dataY=[],[]
    for i in range(len(dataset)-look_back):
        a=dataset[i:(i+look_back),0]
        dataX.append(a)
        dataY.append(dataset[i+look_back,0])
    print(len(dataY))
    return np.array(dataX), np.array(dataY)


def main():
    result=import_data()
    print(result.head())
    result['date'] = pd.to_datetime(result['Date'])
    data=result.loc[:, ['Hourly production [kWh]']]
    data=data.set_index(result.date)
    data['Hourly production [kWh]']=pd.to_numeric(data['Hourly production [kWh]'], downcast='float', errors='coerce')
    print(data.head())
    plt.plot(result['date'], data['Hourly production [kWh]'], 'bx-')
    plt.xlabel='Date'
    plt.ylabel='kWh'
    plt.title='Data frame wind kWh production'
    plt.show()
    #resempliranje na kWh kolonata od dataframe-ot po nedela: W
    weekly=data.resample('W').sum()
    weekly.plot(style=[':', '--', '-'])
    plt.title='Resempliranje po nedela'
    plt.show()
    #reselmpliranje po den
    daily=data.resample('D').sum()
    daily.rolling(30,center=True).sum().plot(style=[':','--','-'])
    plt.title='Resempliranje po denovi'
    plt.show()
    #resempliranje po cas
    by_time=data.groupby(data.index.time).mean()
    hourly_ticks=4*60*60*np.arange(6)
    by_time.plot(xticks=hourly_ticks,style=[':','--','-'])
    plt.title='Resempliranje po casovi'
    plt.show()
    print('Resempliranje po cas: ')
    print(by_time.head())
    print('Resempliranje po den: ')
    print(daily.head())
    print('Resempliranje po nedela: ')
    print(weekly.head())

    raw_data_df=pd.read_excel('Wind_power_plant_hourly_2015-2017.xlsx')
    df=raw_data_df.loc[:,['Date','Hourly production [kWh]']]
    df['Hourly production [kWh]']=pd.to_numeric(df['Hourly production [kWh]'],errors='coerce')
    df=df.groupby(['Date']).sum().reset_index()
    print(df.head())

    df.plot.line(x='Date',y='Hourly production [kWh]',figsize=(18,9),linewidth=5,fontsize=20)
    plt.show()

    #Mesecen prikaz
    mon=df['Date']
    temp=pd.DatetimeIndex(mon)
    month=pd.Series(temp.month)
    to_be_plotted=df.drop(['Date'],axis=1)
    to_be_plotted=to_be_plotted.join(month)
    to_be_plotted.plot.scatter(x='Hourly production [kWh]',y='Date',figsize=(16,8),linewidth=5,fontsize=20)
    plt.title='Resempliranje po mesec'
    plt.show()

    #Analiza na trendot na veternata energija
    df['Hourly production [kWh]'].rolling(5).mean().plot(figsize=(20,10),linewidth=5,fontsize=20)
    plt.show()

    #Za varijaciite vo sezoni
    df['Hourly production [kWh]'].diff(periods=30).plot(figsize=(20,20),linewidth=5,fontsize=20)
    plt.show()

    #Korelacija
    pd.plotting.autocorrelation_plot(df['Hourly production [kWh]'])
    plt.show()

    #Prophet se koristi za analiza na vremenski serii i dnevni nabljuduvanja
    #koi prikazuvaat sabloni na pojavuvanje vo razlicni vremenski skaliranja
    #Prophet bi mozel da se iskoristi za predviduvanje bidejki ima intuitivni parametri koi moze da se podesat
    #Pred da go iskoristime Prophet gi imenuvame kolonite za da se sovpagaat so formatot.
    #Kolonata Date mora da se vika 'ds', a vrednosnata kolona sakame da go predvidime 'y'
    #Ke gi iskoristime dnevnite sumarni podatoci:

    df2=daily
    df2.reset_index(inplace=True)
    #Potrebnite koloni za Prophet ds(Date) i y(value)
    df2=df2.rename(columns={'Date':'ds','Hourly production [kWh]':'y'})
    print(df2.head())
    #Sega go importirame fbprophet, kreirame model koj ke se sovpadne so podatocite.
    #Vo prophet changepoint prior scale parametarot se koristi za da kontrolira kolku e senzitiven trendot na promeni, so povisoka vrednost
    #koja znaci deka e posenzitiven i poniska - pomalku cuvstvitelen.
    #Posle probuvanje so poveke parametri, go setiram ovoj parametar na 0.10, od default vrednosta na 0.05
    #Make the prophet model and fit on the data
   # df2_prophet=fbprophet.Prophet(changepoint_prior_scale=0.10)
    #df2_prophet.fit(df2)

    #da napravime dataframe za predviduvanje vo slucajov za eden den
    #df2_forecast=df2_prophet.make_future_dataframe(periods=1,freq='H')
    #Make prediction
   # df2_forecast=df2_prophet.predict(df2_forecast)
    #df2_prophet.plot(df2_forecast,xlabel='Date',ylabel='kWh')
    #plt.title('simple test with prophet')
    #plt.show()

    #Long Short term memory recurrent neural network
    mydata=data.loc[:, ['Hourly production [kWh]']]
    #mydata=mydata.set_index(daily.date)

    #print(mydata.head())
    #LTMS se senzitivni na vleznite podatoci
    #posebno koga se koristi sigmoidna ili tangensova funkcija
    #za aktivacija. Dobra praktika e da se reskaliraat podatocite ili da se normaliziraat
    values = mydata['Hourly production [kWh]'].values.reshape(-1,1)
    values = values.astype('float32')
    scaler = MinMaxScaler(feature_range=(0,1))
    scaled = scaler.fit_transform(values)

    #Podelba na podatocnoto mnozestvo na train i test (80-20)
    train_size=int(len(scaled)*0.8)
    test_size=len(scaled)-train_size
    train,test=scaled[0:train_size,:],scaled[train_size:len(scaled),:]
    print(len(train),len(test))
    #Podgotvuvanje na train i test mnozestvoto za modelot
    look_back=2
    trainX,trainY=create_dataset(train,look_back)
    testX,testY=create_dataset(test,look_back)
    print(len(trainX))
    print(len(testX))
    print(len(trainY))
    print(len(testY))
#LSTM mrezata ocekuva vlez vo oblik:[samples,time steps, features]
#Nasite podatoci momentalno se vo forma [samples,features]
#I go oblikuvame problemot kako dvokraten cekor za sekoj primerok.
#Moze da go transformirame podgotvenoto train i test set vo ocekuvana struktura
    print(len(trainX))
    print(trainX.shape[0])

    trainX = np.reshape(trainX, (trainX.shape[0], 1, trainX.shape[1]))

    testX=np.reshape(testX, (testX.shape[0], 1, testX.shape[1]))

   # trainY = np.reshape(trainY, (trainY.shape[0], 2, trainY.shape[1]))
    #testY=np.reshape(testY, (testY.shape[0], 2, testY.shape[1]))

    #design and fit LSTM network

    model=Sequential()
    model.add(LSTM(100,input_shape=(trainX.shape[1],trainX.shape[2])))
    model.add(Dense(1))
    model.compile(loss='mae',optimizer='adam')
    history=model.fit(trainX,trainY,epochs=300,batch_size=100,validation_data=(testX,testY),verbose=0,shuffle=False)

    #Plotting the loss
    plt.plot(history.history['loss'],label='train')
    plt.plot(history.history['val_loss'],label='test')
    plt.legend()
    plt.show()
    #Presmetuvanje na mape
    print('Vrednost na MAPE')
    print(mean_absolute_percentage_error(history.history['loss'],history.history['val_loss']))


def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


if __name__ == "__main__":
    main()